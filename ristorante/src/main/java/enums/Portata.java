package enums;


public enum Portata {
	PRIMO,
	PIZZE,
	SECONDO,
	CONTORNO,
	DOLCE,
	BIBITE,
	EXTRA
}
