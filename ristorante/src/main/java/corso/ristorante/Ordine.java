package corso.ristorante;

import java.util.ArrayList;
import java.util.List;

public class Ordine {

	private int id_tavolo;
	private List<Pietanza> pietanzeOrdinate;
	private double conto;
	
	public Ordine(){}
	public Ordine(int idTavolo) {
		setId_tavolo(idTavolo);
		setPietanzeOrdinate(new ArrayList<Pietanza>());
		setConto(0);
	}
	public Ordine(int idTavolo, List<Pietanza> ordinate) {
		setId_tavolo(idTavolo);
		setPietanzeOrdinate(ordinate);
		double conto = getConto();
		for (Pietanza pietanza : ordinate) {
			conto +=(pietanza.getCosto());
		}
		setConto(conto);
	}
	
	public int getId_tavolo() {
		return id_tavolo;
	}
	public void setId_tavolo(int id_tavolo) {
		this.id_tavolo = id_tavolo;
	}
	public List<Pietanza> getPietanzeOrdinate() {
		return pietanzeOrdinate;
	}
	public void setPietanzeOrdinate(List<Pietanza> pietanzeOrdinate) {
		this.pietanzeOrdinate = pietanzeOrdinate;
	}
	public double getConto() {
		return conto;
	}
	public void setConto(double conto) {
		this.conto = conto;
	}
	
}
