package corso.ristorante;

import java.util.List;
import java.util.stream.Collectors;

public class Comanda {

	private int idTavolo;
	private List<Pietanza> pietanzeOrdinate;
	
	public Comanda(int tavolo, List<Pietanza> ordine) {
		this.pietanzeOrdinate = ordine;
	}
	
	public double getConto() {
		return pietanzeOrdinate.stream().mapToDouble(x-> x.getCosto()).sum();
		
	}
}
