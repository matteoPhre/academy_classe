package corso.ristorante;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Menu {

	private List<Pietanza> pietanze;

	public Menu() {
		pietanze = new LinkedList<Pietanza>();
	}

	public List<Pietanza> getPietanze() {
		return pietanze;
	}

	public Pietanza getPietanzaByNome(String nomePietanzaDaCercare) {
		List<Pietanza> pietanzeNelMenu = getPietanze();
		Pietanza pietanza = null;
		Optional<Pietanza> pietanzaOpzionale = pietanzeNelMenu.stream()
				.filter(x -> x.getNome().equals(nomePietanzaDaCercare)).findFirst();

		if (pietanzaOpzionale.isPresent()) {
			pietanza = pietanzaOpzionale.get();
		}
		return pietanza;
	}

	public void addPietanza(Pietanza p) {
		this.pietanze.add(p);
	}

}
