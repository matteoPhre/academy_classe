package corso.ristorante;

import enums.Portata;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
	public void testGenerazioneTavoli() {
		Ristorante daNino = new Ristorante (20);
		daNino.generazioneTavoli();
		assert(daNino.getTavolo(3).getNumero() == 3);
	}
	public void testPrenotazione() {
		Ristorante daNino = new Ristorante (20);
		daNino.generazioneTavoli();
		Cliente nino = new Cliente ("Nino",500);
		nino.prenotaTavolo(daNino, 1);
		daNino.prenotaTavolo(nino);
		assert(daNino.getTavolo(0).getOccupazione() == true);
	}
	public void testVaiAlRistorante() {
		Ristorante daNino = new Ristorante (20);
		daNino.generazioneTavoli();
		Cliente nino = new Cliente ("Nino",500);
		nino.prenotaTavolo(daNino, 1);
		daNino.prenotaTavolo(nino);
		assert(daNino.getTavolo(0).getOccupazione() == true);
	}
	public void testVaiAlRistoranteSenzaPrenotazione() {
		Ristorante daNino = new Ristorante (20);
		daNino.generazioneTavoli();
		Cliente nino = new Cliente ("Nino",500);
		daNino.vaiAlRistorante(nino, 2);
		assert(daNino.getTavolo(16).getOccupazione() == true);
	}
	
	public void testMenu() {
		Menu m = new Menu();
		
		m.addPietanza(new Pietanza("Carbonara","passta con uovo, pancetta e parmiggiano",5.0,Portata.PRIMO));
		m.addPietanza(new Pietanza("Pasta al ragù","Pasta con ragù alla bolognese",5.0,Portata.PRIMO));
		m.addPietanza(new Pietanza("Pizza salsiccia","Pizza conj salsiccia DOP",5.0,Portata.PIZZE));
		m.addPietanza(new Pietanza("Acqua naturale 1l","",1.0,Portata.BIBITE));
		m.addPietanza(new Pietanza("Profiterol","Bighe al cioccolato immersi nel cioccolato",5.0,Portata.DOLCE));
		
		assert(m.getPietanzaByNome("Pasta al ragù").getNome().equals("Pasta al ragù"));
	}
	
}
