package corso.ristorantino.model;

public class Pietanza {
	private String nomePietanza;
	private double prezzo;
	private TipoDiPietanza tipo;
	
	
	public String getNomePietanza() {
		return nomePietanza;
	}
	public void setNomePietanza(String nomePietanza) {
		this.nomePietanza = nomePietanza;
	}
	public double getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	public TipoDiPietanza getTipo() {
		return tipo;
	}
	public void setTipo(TipoDiPietanza tipo) {
		this.tipo = tipo;
	}
	
	public Pietanza(String nomePietanza, double prezzo, TipoDiPietanza tipo){
		this.nomePietanza = nomePietanza;
		this.prezzo = prezzo;
		this.tipo = tipo;		
	}
}
