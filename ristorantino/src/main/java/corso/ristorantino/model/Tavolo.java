package corso.ristorantino.model;

import java.util.ArrayList;
import java.util.List;

public class Tavolo {
	private int idTavolo;
	private List<Pietanza> comanda;
	private double conto;
	private int nPosti;
	private boolean occupato;
	
	public int getIdTavolo() {
		return idTavolo;
	}
	public void setIdTavolo(int idTavolo) {
		this.idTavolo = idTavolo;
	}
	public List<Pietanza> getComanda() {
		return comanda;
	}
	public void setComanda(List<Pietanza> comanda) {
		this.comanda = comanda;
	}
	public double getConto() {
		return conto;
	}
	public void setConto(double conto) {
		this.conto = conto;
	}
	public int getnPosti() {
		return nPosti;
	}
	public void setnPosti(int nPosti) {
		this.nPosti = nPosti;
	}
	public boolean isOccupato() {
		return occupato;
	}
	public void setOccupato(boolean occupato) {
		this.occupato = occupato;
	}
	
	public Tavolo(int idTavolo){
		setIdTavolo(idTavolo);
		setComanda(null);
		setOccupato(false);
		setConto(0);
		setnPosti(0);
	}
	
	
}
