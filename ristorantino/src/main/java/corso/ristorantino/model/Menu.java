package corso.ristorantino.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Menu {
	private String nomeMenu;
	private List<Pietanza> listona;

	public String getNomeMenu() {
		return nomeMenu;
	}

	public void setNomeMenu(String nomeMenu) {
		this.nomeMenu = nomeMenu;
	}

	public Menu(String nome) {
		setNomeMenu(nome);
		setListona(new ArrayList<Pietanza>());
	}

	public Menu(String nome, List<Pietanza> lista) {
		setNomeMenu(nome);
		setListona(lista);
	}

	public List<Pietanza> getListona() {
		return listona;
	}

	public void setListona(List<Pietanza> listona) {
		this.listona = listona;
	}

	public Pietanza getPietanzaByNome(String nomePietanza) {
		List<Pietanza> listaPietanze = getListona();
		Pietanza daTornare = null;
		Optional<Pietanza> pietanzaOpzionale = listaPietanze.stream().filter(pietanza -> pietanza.getNomePietanza().equalsIgnoreCase(nomePietanza)).findFirst();
		if(pietanzaOpzionale.isPresent()) {
			daTornare = pietanzaOpzionale.get();
		}
		return daTornare;
	}

	// public Pietanza getPietanzaByNome(String n){
	// List<Pietanza> p = getListona();
	// Optional<Pietanza>pietanza = p.stream().filter(x ->
	// x.getNomePietanza().equals(n)).findFirst();
	// if(pietanza.isPresent()){
	// return pietanza.get();
	// }else{
	// return null;
	// }
	// }
}
