package corso.ristorantino.model;

public enum TipoDiPietanza {
	Antipasto,
	Primo,
	Secondo,
	Dolce;
}
