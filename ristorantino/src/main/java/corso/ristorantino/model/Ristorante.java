package corso.ristorantino.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Ristorante {
	private String nomeRistorante;
	private int nTavoli;
	private Menu menu;
	private List<Tavolo> tavoli;
	private int nPrenotati;

	public String getNomeRistorante() {
		return nomeRistorante;
	}

	public void setNomeRistorante(String nomeRistorante) {
		this.nomeRistorante = nomeRistorante;
	}

	public int getnTavoli() {
		return nTavoli;
	}

	public void setnTavoli(int nTavoli) {
		this.nTavoli = nTavoli;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public List<Tavolo> getTavoli() {
		return tavoli;
	}

	public void setTavoli(List<Tavolo> tavoli) {
		this.tavoli = tavoli;
	}

	public int getnPrenotati() {
		return nPrenotati;
	}

	public void setnPrenotati(int nPrenotati) {
		this.nPrenotati = nPrenotati;
	}

	public Ristorante(String nome, int nTavoli) {
		this.nomeRistorante = nome;
		this.nTavoli = nTavoli;
		tavoli = new ArrayList<Tavolo>();
		for (int i = 1; i < nTavoli+1; i++) {
			Tavolo t = new Tavolo(i);
			tavoli.add(t);
		}
		setnPrenotati(0);
	}

	public int prenotaTavolo(int nPosti) {
		int idLibero = 0;
		if (getnPrenotati() < nTavoli) {
			Optional<Tavolo> tavoloLibero = tavoli.stream().filter(x -> x.isOccupato() == false)
					.findFirst();
			
			if(tavoloLibero.isPresent()) {
				Tavolo t = tavoloLibero.get();
				idLibero = t.getIdTavolo();
				t.setOccupato(true);
				t.setnPosti(nPosti);
				setnPrenotati(getnPrenotati() + 1);
			}
		}
		return idLibero;
	}

	public void comanda(int id, String... pietanzeOrdinate) {
		double conto = 0;
		Menu m = getMenu();
		List<Pietanza> ordinate = new ArrayList<Pietanza>();

		for (int i = 0; i < pietanzeOrdinate.length; i++) {
			Pietanza p = m.getPietanzaByNome(pietanzeOrdinate[i]);
			if(p != null){
				ordinate.add(p);
				conto += p.getPrezzo();	
			}
			
		}

		Tavolo t = getTavoli().stream().filter(x -> x.getIdTavolo() == id).findFirst().get();
		t.setConto(conto);
		t.setComanda(ordinate);
	}

	public double pagaConto(int idTavolo) {
		Tavolo t = getTavoli().stream().filter(x -> x.getIdTavolo() == idTavolo).findFirst().get();
		double scontrino = t.getConto();
		t.setConto(0);
		t.setOccupato(false);
		t.setComanda(new ArrayList<Pietanza>());
		t.setnPosti(0);
		setnPrenotati(getnPrenotati()-1);
		return scontrino;
	}


}