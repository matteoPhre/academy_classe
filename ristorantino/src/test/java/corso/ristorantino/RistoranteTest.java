package corso.ristorantino;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import corso.ristorantino.model.Menu;
import corso.ristorantino.model.Pietanza;
import corso.ristorantino.model.Ristorante;
import corso.ristorantino.model.Tavolo;
import corso.ristorantino.model.TipoDiPietanza;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RistoranteTest extends TestCase {

	private Ristorante _sut;
	private List<Tavolo> tavoli;
	private List<Pietanza> listaPietanze;
	private Menu menu;
	private String nomeRistorante;
	private int numeroTavoli;

	private Pietanza pietanza;
	private Tavolo tavolo;

	private void setUpRistorante() {
		numeroTavoli = 1;
		tavoli = new ArrayList<Tavolo>();
		listaPietanze = new ArrayList<Pietanza>();
		pietanza = new Pietanza("NOME", 1, TipoDiPietanza.Antipasto);
		listaPietanze.add(pietanza);
		tavolo = new Tavolo(1);
		menu = new Menu("MENU", listaPietanze);
	}

	public RistoranteTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(RistoranteTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testCreazioneRistorante() {
		nomeRistorante = "Nonna Rosa";
		numeroTavoli = 5;
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		boolean isType = _sut instanceof Ristorante;
		assertTrue(isType);

		assertEquals(nomeRistorante, _sut.getNomeRistorante());
		assertEquals(numeroTavoli, _sut.getnTavoli());
		assertTrue(!_sut.getTavoli().isEmpty());
		assertEquals(numeroTavoli, _sut.getTavoli().size());
		assertEquals(0, _sut.getnPrenotati());

	}

	public void testPrenotaTavolo_successfully() {
		setUpRistorante();
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		_sut.setMenu(menu);

		int idTavolo = _sut.prenotaTavolo(2);
		assertEquals(tavolo.getIdTavolo(), idTavolo);
	}

	public void testPrenotaTavolo_failure() {
		setUpRistorante();
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		_sut.setMenu(menu);
		for (Tavolo t : _sut.getTavoli()) {
			t.setOccupato(true);
		}
		_sut.setnPrenotati(_sut.getnTavoli());

		int idTavolo = _sut.prenotaTavolo(2);
		assertEquals(0, idTavolo);
	}

	public void testComanda_successfully() {
		setUpRistorante();
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		_sut.setMenu(menu);

		int idTavolo = _sut.prenotaTavolo(2);
		String[] listaOrdine = new String[] { pietanza.getNomePietanza() };
		_sut.comanda(idTavolo, listaOrdine);
		Optional<Tavolo> opt = _sut.getTavoli().stream().filter(x -> x.getIdTavolo() == idTavolo).findFirst();

		if (opt.isPresent()) {
			Tavolo t = opt.get();
			assertEquals(pietanza.getPrezzo(), t.getConto());
			assertEquals(listaOrdine.length, t.getComanda().size());
			assertEquals(listaOrdine[0],
					t.getComanda().stream().filter(x -> x.getNomePietanza().equals(pietanza.getNomePietanza()))
							.findFirst().get().getNomePietanza());
			assertTrue(t.getComanda().stream()
					.filter(x -> x.getNomePietanza().equals(pietanza.getNomePietanza())) != null);

		}

	}

	public void testComanda_failure_EmptyArray() {
		setUpRistorante();
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		_sut.setMenu(menu);

		int idTavolo = _sut.prenotaTavolo(2);
		String[] listaOrdine = new String[] {};
		_sut.comanda(idTavolo, listaOrdine);
		Optional<Tavolo> opt = _sut.getTavoli().stream().filter(x -> x.getIdTavolo() == idTavolo).findFirst();

		if (opt.isPresent()) {
			Tavolo t = opt.get();
			assertTrue(t.getComanda().isEmpty());
			assertTrue(t.getConto() == 0);
		}

	}
	
	public void testComanda_failure_Not_existing_Item() {
		setUpRistorante();
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		_sut.setMenu(menu);

		int idTavolo = _sut.prenotaTavolo(2);
		String[] listaOrdine = new String[] {"BANANAbyFRANCY"};
		_sut.comanda(idTavolo, listaOrdine);
		Optional<Tavolo> opt = _sut.getTavoli().stream().filter(x -> x.getIdTavolo() == idTavolo).findFirst();

		if (opt.isPresent()) {
			Tavolo t = opt.get();
			assertTrue(t.getComanda().isEmpty());
			assertTrue(t.getConto() == 0);
		}

	}
	
	public void testPagaConto(){
		setUpRistorante();
		_sut = new Ristorante(nomeRistorante, numeroTavoli);
		_sut.setMenu(menu);

		int idTavolo = _sut.prenotaTavolo(2);
		String[] listaOrdine = new String[] { pietanza.getNomePietanza() };
		_sut.comanda(idTavolo, listaOrdine);
		Optional<Tavolo> opt = _sut.getTavoli().stream().filter(x -> x.getIdTavolo() == idTavolo).findFirst();

		if(opt.isPresent()){
			Tavolo t = opt.get();
			
			double contoTot = t.getConto();
			int numeroP = _sut.getnPrenotati();
			double scontrino = _sut.pagaConto(idTavolo);
			
			assertEquals(contoTot, scontrino);
			assertEquals(0.0, t.getConto());
			assertEquals(false, t.isOccupato());
			assertTrue(t.getComanda().isEmpty());
			assertEquals(0, t.getnPosti());
			assertTrue(numeroP > _sut.getnPrenotati());
		}
	}
}
