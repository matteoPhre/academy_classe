package corso.ristorantino;

import java.util.ArrayList;
import java.util.List;

import corso.ristorantino.model.Menu;
import corso.ristorantino.model.Pietanza;
import corso.ristorantino.model.TipoDiPietanza;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MenuTest extends TestCase {

	private Menu _sut;
	
	public List<Pietanza> lista;
	public Pietanza toAdd;
	public Pietanza retrieved;
	public String nomeMenu;
	
	public MenuTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(MenuTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testMenuCreation() {
		String nomeMenu ="MENU DI MARE"; 
		_sut = new Menu(nomeMenu);
		boolean isSameType = _sut instanceof Menu;
		assertTrue(isSameType);
		
		assertEquals(nomeMenu, _sut.getNomeMenu());
		assertTrue(_sut.getListona().isEmpty());
	}
	
	public void testMenuCreationWithPopulatedList() {
		setUpMenu();
		
		_sut = new Menu(nomeMenu, lista);
		boolean isSameType = _sut instanceof Menu;
		assertTrue(isSameType);
		
		assertEquals(nomeMenu, _sut.getNomeMenu());
		assertTrue(!_sut.getListona().isEmpty());
		assertTrue(_sut.getListona().size() > 0);
		
		retrieved = lista.stream().findFirst().get();
		
		assertEquals(retrieved.getNomePietanza(), toAdd.getNomePietanza());
		assertEquals(retrieved.getPrezzo(), toAdd.getPrezzo());
		assertEquals(retrieved.getTipo(), toAdd.getTipo());
	}

	private void setUpMenu() {
		nomeMenu = "MENU";
		lista = new ArrayList<Pietanza>();
		toAdd = new Pietanza("NOME", 1, TipoDiPietanza.Antipasto);
		lista.add(toAdd);
	}
	
	public void testMenuGetPietanzaByNome() {
		setUpMenu();
		_sut = new Menu(nomeMenu, lista);
		
		retrieved = _sut.getPietanzaByNome(toAdd.getNomePietanza());
		boolean isSameType = retrieved instanceof Pietanza;
		assertTrue(isSameType);
	}
	
	public void testMenuGetPietanzaByNomeUpperCase() {
		setUpMenu();
		_sut = new Menu(nomeMenu, lista);
		
		retrieved = _sut.getPietanzaByNome(toAdd.getNomePietanza().toUpperCase());
		boolean isSameType = retrieved instanceof Pietanza;
		assertTrue(isSameType);
	}
	
	public void testMenuGetPietanzaByNomeLowerCase() {
		setUpMenu();
		_sut = new Menu(nomeMenu, lista);
		
		retrieved = _sut.getPietanzaByNome(toAdd.getNomePietanza().toLowerCase());
		boolean isSameType = retrieved instanceof Pietanza;
		assertTrue(isSameType);
	}
}
