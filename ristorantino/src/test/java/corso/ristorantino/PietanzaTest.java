package corso.ristorantino;

import corso.ristorantino.model.Pietanza;
import corso.ristorantino.model.TipoDiPietanza;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PietanzaTest extends TestCase {
	
	public Pietanza _sut;
	
	public PietanzaTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(PietanzaTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testPietanzaCreation() {
		_sut = new Pietanza("NOME", 1, TipoDiPietanza.Antipasto);
		boolean isSameType = _sut instanceof Pietanza;
		assertTrue(isSameType);
	}
	
	public void testPietanzaCreationAndCheckProperties() {
		String nomePietanza = "NOME";
		double prezzo = 1;
		TipoDiPietanza tipo = TipoDiPietanza.Antipasto;
		_sut = new Pietanza(nomePietanza, prezzo, tipo);
		assertEquals(_sut.getNomePietanza(), nomePietanza);
		assertEquals(_sut.getPrezzo(), prezzo);
		assertEquals(_sut.getTipo(), tipo);
	}
}
