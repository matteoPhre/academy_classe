package corso.hibernatehelloworld.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    //...
 
    @OneToOne(mappedBy = "address")
    private Student student;

	public Student getStudent() {
		return student;
	}
	
	@Column(name = "CITY", nullable = false)
	private String city;
	
	@Column(name = "STREET", nullable = false)
	private String street;
	
	@Column(name="number", nullable=false)
	private int number;
	
	@Column(name="ZIP", nullable=false)
	private String ZIP;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getZIP() {
		return ZIP;
	}

	public void setZIP(String zIP) {
		ZIP = zIP;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
}