package corso.hibernatehelloworld.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Student_Details")//one-to-many mapping means that one row in a table is mapped to multiple rows in another table.

public class StudentDetails {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NOME_PADRE", nullable=false)
	private String nomePadre;
	
	public String getNomePadre() {
		return nomePadre;
	}

	public void setNomePadre(String nomePadre) {
		this.nomePadre = nomePadre;
	}

	public String getNomeMadre() {
		return nomeMadre;
	}

	public void setNomeMadre(String nomeMadre) {
		this.nomeMadre = nomeMadre;
	}

	@Column(name="NOME_MADRE", nullable=false)
	private String nomeMadre;
	
	//@ManyToOne
    //@JoinColumn(name="studente_id", nullable=false)
    //private Student studente_esame;

	//public Student getStudente_esame() {
		//return studente_esame;
	//}

	//public void setStudente_esame(Student studente_esame) {
		//this.studente_esame = studente_esame;
	//}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
