package corso.hibernatehelloworld.model;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="STUDENT")
public class Student implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="FIRST_NAME", nullable= false)
	private String firstName;
	@Column(name = "LAST_NAME", nullable = false)
	private String lastName;
	@Column(name="SECTION", nullable=false)
	private String section;
	
	//@OneToMany(mappedBy="studente_esame")
	
	@OneToMany(
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<StudentDetails> corsi;
	
	public Student() {
		this.corsi = new ArrayList<>();
	}
	
	public List<StudentDetails> getCorsi() {
		return corsi;
	}

	public void setCorsi(List<StudentDetails> corsi) {
		this.corsi = corsi;
	}

	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "id")
    private Address address;
		
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(!(obj instanceof Student)) return false;
		Student other = (Student) obj;
		if(id != other.id) 
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return MessageFormat.format("[id={0}; firstName={1}; lastName={2}; section={3}]",
				getId(), getFirstName(), getLastName(), getSection());
	}
}
