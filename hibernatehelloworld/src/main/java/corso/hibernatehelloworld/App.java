package corso.hibernatehelloworld;

import corso.hibernatehelloworld.model.Student;
import corso.hibernatehelloworld.service.StudentService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import corso.hibernatehelloworld.manager.HibernateManagerUtil;
import corso.hibernatehelloworld.manager.HibernateUtil;;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //App application = new App();
    	StudentService application = new StudentService();
    	
        int saved1 = application.saveStudent("Gianni", "Gggg1", "WWWPPP1");
        int saved2 = application.saveStudent("Zucca", "Gggg2", "WWWPPP2");
        int saved3 = application.saveStudent("Ciccio", "Gggg3", "WWWPPP3");
        
        List<Student> students = application.getAllStudents();
        
        for (Student student : students) {
			System.out.println(student.toString());
		}
        
        Student student = application.getSingleStudent(saved2);
        System.out.println("Single student: "+ student.toString());
        
        
        application.updateUserById(saved3, "SECTION_POST_UPDATE");
        
        Student studentUpdated = application.getSingleStudent(saved3);
        System.out.println("Single student updated: "+ studentUpdated.toString());
        
        System.out.println("SIZE LIST PRE-DELETE: " + students.size());
        
        application.deleteStudent(saved3);
        
        int sizeAfterDelete = application.getAllStudents().size();
        
        System.out.println("LIST SIZE AFTER-DELETE: " + sizeAfterDelete );

        System.exit(0);
    }
    
    
    public List<Student> findAllStudentsWithCriteriaQuery(String property, String value) {
    	CriteriaBuilder builder = HibernateUtil.getCriteriaBuilder();
		EntityManager em = HibernateUtil.getEntityManager();
		CriteriaQuery<Student> criteriaQuery = builder.createQuery(Student.class);
		Root<Student> studentRoot = criteriaQuery.from(Student.class);
		criteriaQuery.select(studentRoot);
		criteriaQuery.where(builder.equal(studentRoot.get(property),value));
		List<Student> students = em.createQuery(criteriaQuery).getResultList();
		return students;
    }
}
