package corso.hibernatehelloworld.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import corso.hibernatehelloworld.manager.HibernateManagerUtil;
import corso.hibernatehelloworld.model.Address;
import corso.hibernatehelloworld.model.StudentDetails;
import corso.hibernatehelloworld.model.Student;

public class StudentService  {

	public int saveStudent(String firstName, String lastName, String section) {
    	Student student = new Student();
    	student.setFirstName(firstName);
    	student.setLastName(lastName);
    	student.setSection(section);
    	
    	//ONE-TO-ONE RELATIONSHIP BETWEEN STUDENT AND ADDRESS
    	Address addr = new Address();
    	addr.setNumber(1);
    	addr.setStreet("VIA CIAONE");
    	addr.setZIP("123123");
    	addr.setCity("CHICAGO");
    	student.setAddress(addr);
    	
    	
    	//UNIDIRECTIONAL 
    	//ONE-TO-MANY RELATION BETWEEN STUDENT AND CORSO
   	
    	StudentDetails corso1  = new StudentDetails();
    	corso1.setNomePadre("AAA_1_PADRE");
    	corso1.setNomeMadre("AAA_1_MADRE");
    	StudentDetails corso2 = new StudentDetails();
    	corso2.setNomePadre("AAA_2_PADRE");
    	corso2.setNomeMadre("AAA_2_MADRE");
    	List<StudentDetails> corsi = new ArrayList<>();
    	corsi.add(corso1);
    	corsi.add(corso2);
    	student.setCorsi(corsi);
    	
    	

    	Session session = HibernateManagerUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	int id = (Integer) session.save(student);
    	session.getTransaction().commit();
    	session.close();
    	return id;
    }
    
    
    
    public Student saveStudentAndRetrieve(String firstName, String lastName, String section) {
    	Student student = new Student();
    	student.setFirstName(firstName);
    	student.setLastName(lastName);
    	student.setSection(section);
    	
    	Session session = HibernateManagerUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	int id = (Integer) session.save(student);
    	session.getTransaction().commit();
    	session.close();
    	student.setId(id);
    	return student;
    }
    
    //Retrieve all students, ordered by firsName ASC
    public List<Student> getAllStudents() {
    	Session session = HibernateManagerUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	
    	String query = "FROM Student as s ORDER BY s.firstName ASC";
    	List<Student> list = (List<Student>)session.createQuery(query).list();
    	session.getTransaction().commit();
    	session.close();
    	return list;	
    }
    
    //Retrieve Student by id passed as parameter
    public Student getSingleStudent(int id) {
    	Session session = HibernateManagerUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	//String query = "FROM Student as s WHERE s.id = " + id;
    	//Student student = (Student) session.createQuery(query);
    	Student student = (Student) session.get(Student.class, id); 
    	session.getTransaction().commit();
    	session.close();
    	return student;
    }
    
    public void updateUserById(int id, String section) {
    	Session session = HibernateManagerUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	Student student = (Student) session.get(Student.class, id);
    	student.setSection(section);
    	//session.update(student); non need to update manually
    	session.getTransaction().commit();
    	session.close();
    }
    
    public void deleteStudent(int id) {
    	Session session = HibernateManagerUtil.getSessionFactory().openSession();
    	session.beginTransaction();
    	Student student = (Student) session.get(Student.class, id);
    	session.delete(student);
    	session.getTransaction().commit();
    	session.close();
    }
}
