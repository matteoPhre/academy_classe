package corso.hibernatehelloworld.service;

import java.util.List;

public interface IService<T> {

	List<T> getAll();
	T getSingle(int cicciopagliaccio);
	int save(String...args);
	T saveAndRetrieve(String...args);
}
