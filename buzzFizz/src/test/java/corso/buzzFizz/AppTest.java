package corso.buzzFizz;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import corso.buzzFizz.App;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	public App _sut;//property che sta ad identificare il system under test
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
        _sut = new App();
    }
    
    /**
     * Rigourous Test :-)
     */
    public void testAppNumeriPositivi()
    {
    	List<Integer> list = new ArrayList<>();
    	int maxValue = 1;
    	list.add(0);
    	list.add(maxValue);
        int result = _sut.cercaIlMaggiore(list);
        assertEquals(maxValue, result);
    }
    
    public void testAppNumeriNegativi() {
    	List<Integer> list = new ArrayList<>(Arrays.asList(-2,-3,-4,-5,-6));
    	int maxValue = -1;
    	list.add(maxValue);
    	int result = _sut.cercaIlMaggiore(list);
    	assertEquals(maxValue, result);
    }
    
    public void testAppNumeriPositiviENegativi() {
    	List<Integer> list = new ArrayList<>(Arrays.asList(0,1,-2,3,-4,5,-6));
    	int maxValue = 7;
    	list.add(maxValue);
    	int result = _sut.cercaIlMaggiore(list);
    	assertEquals(maxValue, result);
    }
    
    public void testAppReadFileToInteger() throws IOException {
    	String filePath = "C:\\Users\\admin\\workspace\\buzzFizz\\target\\classes\\listanumeri.txt";
    	List<Integer> result = _sut.readFileWithoutStream(filePath);
    	
    	assertEquals(13, result.size());
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

}
