package corso.buzzFizz;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import corso.buzzFizz.FizzBuzzService;

/*Numeri da 1 a 100;
 * se numero è multiplo di 3 stampa Fizz
 * se numero è multiplo di 5 stampa Buzz
 * se numero è multiplo di entrambi stampa FizzBuzz
 * altrimenti stampa il numero
 * */

public class FizzBuzzServiceTest  extends TestCase {
	FizzBuzzService _sut;
	
	public FizzBuzzServiceTest( String testName )
    {
        super( testName );
        _sut = new FizzBuzzService();
    }
		

	public static Test suite()
    {
        return new TestSuite( FizzBuzzServiceTest.class );
    }
	

	public void testMultiploTre()
    {
		int x = 15;
        assertTrue(_sut.modulo_di_tre(x));    
        //Non ci interessa che ritorni Fizz 
    }
	

	public void testMultiploCinque()
    {
		int x = 15;
        assertTrue(_sut.modulo_di_tre(x));    
        //Non ci interessa che ritorni Buzz
    }
	
	public void testMultiploCinque_Tre()
    {
		int x = 30;
        assertTrue(_sut.modulo_di_tre(x)); 
        assertTrue(_sut.modulo_di_cinque(x)); 
        //Non ci interessa che ritorni FizzBuzz
    }
	
	public void testNonMultiplo()
    {
		int x = 76;
        assertFalse(_sut.modulo_di_tre(x)); 
        assertFalse(_sut.modulo_di_cinque(x)); 
        //Non ci interessa che ritorni il numero
    }
	
	
	
	
}


