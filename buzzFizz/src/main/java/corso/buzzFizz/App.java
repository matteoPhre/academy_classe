package corso.buzzFizz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.soap.MessageFactory;

/**
 * Hello world!
 *
 */
public class App {
	/*
	 * Il metodo cercaIlMaggiore cerca il maggiore all'interno di un array di
	 * int
	 */
	static List<Integer> list = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		String filePath = "C:\\Users\\admin\\workspace\\buzzFizz\\target\\classes\\listanumeri.txt";
		List<Integer> listaNumeri = readFileWithoutStream(filePath);

		for (Integer integer : listaNumeri) {
			list.add(integer);
		}
		int max = cercaIlMaggiore(list);

		System.out.println(MessageFormat.format("Il maxValue = {0}", max));
	}

	public static List<Integer> readFileToInteger(String filename) throws IOException {
		List<Integer> list = new ArrayList<>();
		try (Stream<String> stream = (Files.lines(Paths.get(filename)))) {
			String primaRiga = "";

			
			Stream<String> toIterate = Stream.of(primaRiga.split(","));
			list = toIterate.map(Integer::parseInt).collect(Collectors.toList());
			// }

		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static List<Integer> readFileWithoutStream(String filename) {
		File file = new File(filename);
		BufferedReader buff = null;
		List<Integer> list = new ArrayList<>();
		try {
			buff = new BufferedReader(new FileReader(file));
			String text = null;
			
			while((text = buff.readLine()) != null) {
				String[] textSplitted = text.split(",");
				for (String value : textSplitted) {
					list.add(Integer.parseInt(value.trim()));
					System.out.println("PARSED VALUE: "+Integer.parseInt(value));
				}
			}
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	public static Integer cercaIlMaggiore(List<Integer> list) {
		// TODO Auto-generated method stub
		int maxVal = list.get(0);
		for (Integer integer : list) {

			if (integer > maxVal) {
				maxVal = integer;
			}
		}
		return maxVal;
	}
}
