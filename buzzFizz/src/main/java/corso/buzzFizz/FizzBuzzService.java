package corso.buzzFizz;

public class FizzBuzzService {

	public void calculateAndPrint(IPrinter printer) {
		for (int i=1; i<101;i++){
			String result = "";
			if(modulo_di_tre(i)){
				result+="Fizz";
			}
			if (modulo_di_cinque(i)){
				result+="Buzz";
			}
			if (!modulo_di_tre(i) && !modulo_di_cinque(i)){
				result+="Il numero è "+ i;
			}
			printer.print(result);		
			//System.out.println(result);
		}
	}
	
	public static boolean modulo_di_tre(int i) {

		if (i % 3 == 0) {
			return true;
		} else
			return false;

	}

	public static boolean modulo_di_cinque(int i) {

		if (i % 5 == 0) {
			return true;
		} else
			return false;

	}
}
