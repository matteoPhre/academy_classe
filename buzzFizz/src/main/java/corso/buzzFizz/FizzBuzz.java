package corso.buzzFizz;

public class FizzBuzz {

	static IPrinter printer;
	static FizzBuzzService fizzBuzzService = new FizzBuzzService();
	/*
	 * Numeri da 1 a 100; se numero è multiplo di 3 stampa Fizz se numero è
	 * multiplo di 5 stampa Buzz se numero è multiplo di entrambi stampa
	 * FizzBuzz altrimenti stampa il numero
	 */
	public static void main(String[] args) {
		printer = new ConsolePrinter();
		
		fizzBuzzService.calculateAndPrint(printer);
	}

}