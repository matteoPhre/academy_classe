import java.text.MessageFormat;
import java.util.Scanner;

public class Main {
	
	private static int resto(int a, int b){
		if (a%b==0){
			return 1;
		} 
			return 0;
	}
	
	private static int resto(int a, int b, int c, String ciccio){
		if (a%b==0){
			return 1;
		} 
			return 0;
	}
	
	private static boolean div(int a, int b){
		if(a%b==0){
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		/*for (int i=0; i<args.length; i++){
		//System.out.print("Inserisci un numero");
		//int a = Input.readDouble();
		//	
			int a=Integer.parseInt(args[i]);
			int b=Integer.parseInt(args[i+1]);
			resto(a, b);
		}*/
		
		Scanner a = new Scanner(System.in);//apertura scanner
		System.out.println("inserire il primo valore");
		int i= a.nextInt();//lettura scanner input
		System.out.println("inserire il secondo valore");
		int c= a.nextInt();//lettura scanner input
		//resto (i, c);
		int rInt = resto(i, c);
		boolean r= div (i, c);
		a.close();//chiusura scanner!
		//messageFormat permette di creare una stringa pattern con placeholder
		System.out.println(MessageFormat.format("il risultato �: {0}", r));
	}
}
