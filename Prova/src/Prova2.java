import java.util.Scanner;

public class Prova2 {
	
	public static boolean rispondimi (int x, boolean y){
		boolean risposta1=false;
		// return ((x%3==0)&&(y==false))?(true):(false)
		if((x%3==0)&&(y==false)){
			risposta1=true;
		}
		return risposta1;
	}

	public static boolean rispondimiPiuFigo (int x, boolean y){
		return ((x%3==0)&&(y==false))?(true):(false);
	}
		
		
	public static void main(String[] args) {
		
		Scanner tastiera = new Scanner (System.in);
		System.out.println("Dammi un intero");
		int a = tastiera.nextInt();
		System.out.println("Dammi un booleano");
		boolean b = tastiera.nextBoolean();
		tastiera.close();
		
		System.out.println(rispondimi(a,b));
		System.out.println(rispondimiPiuFigo(a,b));

	}

}
