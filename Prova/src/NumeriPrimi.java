
public class NumeriPrimi {
	/*Numeri da 1 a 1000, stampare solo i numeri primi
	 * (un numero primo � un numero naturale maggiore di 1 che non ha divisori positivi 
	 * tranne 1 e s� stesso)
	 * */
	public static void main(String[] args) {
		int i = 0;
		int j = 0;
		String primes=" ";
		for (i=1; i<1001; i++) {
			int counter=0;
			for (j=i; j>=1; j--) {
				if (i%j==0) {
				counter+=1;
				}
			}
			if (counter==2) {
				primes += i+" ";
			}
		}
		System.out.println(primes);
	}

}
