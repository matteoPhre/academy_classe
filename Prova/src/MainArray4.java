import java.util.Random;

public class MainArray4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Random rd = new Random();
		int a = rd.nextInt(11);

		int[] array1 = new int[a];
		double[] array1double = new double[a];
		int[] array2 = new int[a];
		double[] array2double = new double[a];
		int[] array3 = new int[a];
		double[] array3double = new double[a];
		
		System.out.println("Array1: ");
		riempi(array1);
		System.out.println(" ");
		riempi(array1double);
		System.out.println(" ");
		System.out.println("Array2: ");
		System.out.println(" ");
		riempi(array2double);
		riempi(array2);
		System.out.println(" ");
		System.out.println("Array3: ");
	
		operazioniTraArray(array1, array2, array3);
		System.out.println(" ");
		operazioniTraArray(array1double, array2double, array3double);

	}

	/*
	 * Il metodo operazioniInArray Effettua la moltiplicazione degli elementi in
	 * posizione pari se hanno un valore dispari altrimenti la divisione Ed
	 * effettua la divisione degli elementi in posizione dispari se hanno un
	 * valore pari altimenti la moltiplicazione ESEMPIO1: ([0,5],[4,5]) =>
	 * [0,25]
	 */
	public static int[] operazioniTraArray(int a[], int b[], int c[]) {

		for (int i = 0; i < a.length; i += 2) {
			if (a[i] % 2 == 0 && b[i] % 2 == 0) {
				c[i] = a[i] * b[i];
			} else
				c[i] = a[i] / b[i];
		}

		for (int i = 1; i < a.length; i += 2) {
			if (a[i] % 2 != 0 && b[i] % 2 != 0) {
				c[i] = a[i] * b[i];
			} else
				c[i] = a[i] / b[i];
		}

		for (int i = 0; i < c.length; i++) {
			System.out.print(" " + c[i]);
		}

		return null;
	}

	public static double[] operazioniTraArray(double a[], double b[], double c[]) {

		for (int i = 0; i < a.length; i += 2) {
			if (a[i] % 2 == 0 && b[i] % 2 == 0) {
				c[i] = a[i] * b[i];

			} else
				c[i] = a[i] / b[i];
		}

		for (int i = 1; i < a.length; i += 2) {
			if (a[i] % 2 != 0 && b[i] % 2 != 0) {
				c[i] = a[i] * b[i];
			} else
				c[i] = a[i] / b[i];
		}

		for (int i = 0; i < c.length; i++) {
			System.out.print(" " + c[i]);
		}

		return null;
	}

	public static void riempi(int n[]) {
		Random rd = new Random();
		for (int i = 0; i < n.length; i++) {
			n[i] = rd.nextInt(20);
			System.out.print(" " + n[i]);

		}
	}

	public static void riempi(double n[]) {
		Random rd = new Random();
		for (int i = 0; i < n.length; i++) {
			n[i] = rd.nextDouble();
			System.out.print(" " + n[i]);

		}
	}

}
