import java.util.Arrays;

public class CesarCypher {

	/*
	 * Data una frase (rimossi gli spazi), cifrarla in base ad una chiave
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String cesare[] = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
				"S", "T", "U", "V", "W", "X", "Y", "Z" };
		String messaggio[] = { "C", "I", "A", "O", "Z", "I", "O" };
		System.out.println(Arrays.toString(cifratore(cesare, messaggio)));

	}

	public static String[] cifratore(String a[], String b[]) {
		int key = 3;
		String cifrato[] = new String[a.length];
		for (int i = 0; i < key; i++) {
			cifrato[a.length-key+i] = a[i];
		}
		for (int j = key;j<a.length;j++){
			cifrato[j-key]= a[j];
		}
		String m[] = new String[b.length];
		for (int i = 0; i < b.length; i++) {
			int bruto = Arrays.asList(a).indexOf(b[i]);
			m[i] = cifrato[bruto];
		}
		return m;
	}

}
