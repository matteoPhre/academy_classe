
public class Array {
	/* Il metodo cercaIlMaggiore
	 * cerca il maggiore all'interno di un array 
	 * */
	public static int cercaIlMaggiore(int a[]) {
		return 0;
	}
	
	/* Il metodo sommaGliArray
	 * restituisce la somma tra i due array
	 * 
	 * */
	public static int[] sommaTraArray(int a[], int b[]) {
		return null;
	}
	
	/* Il metodo arrayTrim
	 * restituisce un sottoarray contenente gli elementi 
	 * che in iesima posizione nel secondo array hanno il valore true
	 * Se il secondo array � sottodimensionato rispetto al primo Si assume true come valore di default 
	 * ESEMPIO1: ("[4,5,7,9],[true,false,true]") => [4,7,9] 
	 * */
	public static int[] arrayTrim(int a[], boolean c[]) {
		
		return null;
	}
	
	/*  Il metodo moltiplicazioneInterna
	 *  Effettua la moltiplicazione tra le due met� di un array
	 *  Se l'array ha dimensione dispari l'elemento centrale viene scartato
	 *  ESEMPIO1: ([4,8,9]) => [36] 
	 *  ESEMPIO2: ([4,8,9,2]) => [8,72]
	 * */
	public static String MoltipicazioneInterna(int a[]) {
		return null;
	}
	
	/*  Il metodo operazioniInArray
	 * 	Effettua la moltiplicazione degli elementi in posizione pari 
	 *  se hanno un valore dispari altrimenti la divisione
	 *  Ed effettua la divisione degli elementi in posizione dispari
	 *  se hanno un valore pari altimenti la moltiplicazione 
	 *  ESEMPIO1: ([0,5],[4,5]) => [0,25]
	 * */
	public static int[] operazioniTraArray(int a[],int b[]) {
		return null;
	}
	
	/*  Il metodo SommaGlieletti
	 *  Il primo array indica la posizione degli elementi dell'array b che possono essere sommati
	 *  Nel caso in cui gli indici sono duplicati o non esistono restituisce un errore
	 *  ESEMPIO1: ([1,0,2],[4,5,6]) => 15
	 * */
	public static int sommaGliEletti(int indici[],int b[]) {
		return 0;
	}
	
	/*  Il metodo numeriMagici 
	 * 	Restituisce un array composto dal maggiore degli elementi in iesima posizione
	 *  Se indici contiene true altrimenti false
	 *  Il valore di default � true
	 *  ESEMPIO1: ([1,0,2],[4,5,6],[true,false,true]) => [4,0,6] 
	 * */
	public static int[] numeriMagici(int a[], int b[], boolean indici[]) {
		return null;
	}
}
