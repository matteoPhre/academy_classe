package product;

import java.text.MessageFormat;

public class Book extends Product {
	String titolo;
	String autore;
	int pagine;
	String ISBN;

	public Book(String titolo, String autore, int pagine, String ISBN, int id, double prezzo, String descrizione,
			double peso, int qta) {
		super(id, prezzo, descrizione, peso, qta);
		this.titolo = titolo;
		this.autore = autore;
		this.pagine = pagine;
		this.ISBN = ISBN;
	}

	public String stringify() {
		return MessageFormat.format("Titolo: {0}; peso: {1}; valore= {2}", 
				this.titolo, this.peso,
				this.prezzo);
	}
}
