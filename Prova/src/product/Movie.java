package product;

import java.text.MessageFormat;

public class Movie extends Product {
	String titolo;
	String regia;
	String lingua;

	public Movie(String titolo, String regia, String lingua, int id, double prezzo, String descrizione, double peso, int qta) {
		super(id,prezzo,descrizione,peso,qta);
		
		this.titolo = titolo;
		this.regia = regia;
		this.lingua = lingua;

	}
	
	public String stringify() {
		Movie movieToStringify = this;
		return MessageFormat.format("Titolo: {0}; peso: {1}; valore= {2}", movieToStringify.titolo, movieToStringify.peso,
				movieToStringify.prezzo);
	}
}
