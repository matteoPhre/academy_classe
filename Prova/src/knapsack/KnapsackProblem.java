package knapsack;

import java.text.MessageFormat;
import java.util.*;

public class KnapsackProblem {
	/*
	 * Given a set of items, each with a weight and a value, we must determine
	 * the number of each item to include in a collection so that the total
	 * weight is less than or equal to a given limit and the total value must be
	 * maximized.
	 */

	private Item[] items;
	private int capacity;

	public KnapsackProblem(Item[] items, int capacity) {
		this.items = items;
		this.capacity = capacity;
	}

	public static void main(String[] args) {
		Item[] items = { new Item("Elt1", 4, 12), new Item("Elt2", 2, 1), new Item("Elt3", 2, 2),
				new Item("Elt4", 1, 1), new Item("Elt5", 10, 4) };

		KnapsackProblem knapsack = new KnapsackProblem(items, 15);
		knapsack.display();
		KnapsackSolution solution = knapsack.solve();
		solution.display();
	}

	public void display() {
		if (items != null && items.length > 0) {
			System.out.println("Knapsack problem");
			System.out.println("Capacity : " + capacity);
			System.out.println("Items :");

			for (Item item : items) {
				System.out.println(MessageFormat.format("- {0}", item.stringify()));
			}
		}
	}

	public KnapsackSolution solve() {
		int NB_ITEMS = items.length;

		// usiamo una matrice per salvare il max value per ogni elemento
		int[][] matrix = new int[NB_ITEMS + 1][capacity + 1];

		// iteriamo sugli elementi
		for (int i = 1; i <= NB_ITEMS; i++) {
			// iteriamo su ogni capacit�
			for (int j = 0; j <= capacity; j++) {
				if (items[i - 1].weight > j) {
					matrix[i][j] = matrix[i - 1][j];
				} else {
					// massimiziamo il valore al rank della matrice
					matrix[i][j] = Math.max(matrix[i - 1][j],
							matrix[i - 1][j - items[i - 1].weight] + items[i - 1].value);
				}
			}
		}

		int res = matrix[NB_ITEMS][capacity];
		int w = capacity;

		// creo array soluzione, di default con 10 elementi(dovrei usare una
		// lista)
		List<Item> itemsSolution = new ArrayList<>();

		// Item[] itemsSolution = new Item[20];

		for (int i = NB_ITEMS; i > 0 && res > 0; i--) {
			if (res != matrix[i - 1][w]) {
				itemsSolution.add(items[i - 1]);
				res -= items[i - 1].value;
				w -= items[i - 1].weight;
			}
		}

		// ritrasformo in array
		Item[] arrayList = new Item[itemsSolution.toArray().length];

		for (int z = 0; z < itemsSolution.toArray().length; z++) {
			arrayList[z] = itemsSolution.get(z);
		}

		return new KnapsackSolution(arrayList, matrix[NB_ITEMS][capacity]);
	}
}
