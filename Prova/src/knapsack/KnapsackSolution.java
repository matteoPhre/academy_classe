package knapsack;

import java.text.MessageFormat;
import java.util.Arrays;

public class KnapsackSolution {
	public Item[] items;
	public int value;
	
	public KnapsackSolution(Item[] items, int value) {
		this.items = items;
		this.value = value;
	}
	
	public void display() {
		if(items != null && !Arrays.asList(items).isEmpty()) {
			System.out.println("\n Knapsack Solution");
			System.out.println(MessageFormat.format("Value = {0}",value));
			System.out.println("Items to pick: ");
			
			for(Item item : items) {
				System.out.println(MessageFormat.format("- {0}", item.stringify()));
			}
			
		}
	}
}
