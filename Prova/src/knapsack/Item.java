package knapsack;

import java.text.MessageFormat;

public class Item {
	public String name;
	public int value;
	public int weight;
	
	public Item(String name, int value, int weight) {
		this.name = name;
		this.value = value;
		this.weight = weight;
	}
	
	public String stringify() {
		return MessageFormat.format("{0} [ value = {1}, weight= {2} ]", name, value, weight);
	}

}
